# FRC Team 2554

## Key Features
This year's code contains subsystems to easily control all aspects of the robot, and a top-of-the-line autonomous system that attracted many cheers at competition.

## Implemented Subsystems
The robot this year was simple but effective, and has three main subsystems
 * Claw
 * Elevator
 * Drive Train

## Autonomous
This year's autonomous uses fine tuned PID controllers for travelling in a straight line and rotating. These controllers are used in sequential command groups to execute autonomous tasks.